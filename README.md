//.....................................................................................................
//.VVVV....VVVVV....................llll...............................................................
//.VVVV....VVVV.....................llll...............................................................
//.VVVV....VVVV.....................llll...............................................................
//.VVVVV..VVVV........eeeeee........llll.......aaaaaa........sssssss.........cccccc..........oooooo....
//..VVVV..VVVV.......eeeeeeee.......llll......aaaaaaaa...... ssssssss.......cccccccc.......ooooooooo...
//..VVVV..VVVV...... eee.eeee.......llll..... aaa.aaaaa..... sss.ssss...... cccc.cccc......oooo.ooooo..
//..VVVVVVVVV....... eee..eeee......llll.........aaaaaa..... ssss.......... ccc..ccc...... ooo...oooo..
//...VVVVVVVV....... eeeeeeeee......llll......aaaaaaaaa......ssssss........ ccc........... ooo...oooo..
//...VVVVVVVV....... eeeeeeeee......llll..... aaaaaaaaa.......sssssss...... ccc........... ooo...oooo..
//...VVVVVVV........ eee............llll..... aaa.aaaaa...........ssss..... ccc..ccc...... ooo...oooo..
//....VVVVVV........ eee..eeee......llll..... aaa.aaaaa..... sss..ssss..... cccc.cccc......oooo.ooooo..
//....VVVVVV.........eeeeeeee.......llll..... aaaaaaaaa..... ssssssss.......ccccccccc......ooooooooo...
//....VVVVV...........eeeeee........llll......aaaaaaaaa.......ssssss.........cccccc..........oooooo....
//.....................................................................................................


Welcome to your Node.js project on Cloud9 IDE!

This chat example showcases how to use `socket.io` with a static `express` server.

## Running the server

1) Open `server.js` and start the app by clicking on the "Run" button in the top menu.

2) Alternatively you can launch the app from the Terminal:

    $ node server.js

Once the server is running, open the project in the shape of 'https://projectname-username.codeanyapp.com/'. 
As you enter your name, watch the Users list (on the left) update. Once you press Enter or Send, the message is shared with all connected clients.
